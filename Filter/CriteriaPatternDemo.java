import java.util.ArrayList;
import java.util.List;

public class CriteriaPatternDemo {
    public static void main(String[] args) {

        List<Person> persons = new ArrayList<Person>();

        persons.add(new Person("R", "Male", "Single"));
        persons.add(new Person("J", "Male", "Married"));
        persons.add(new Person("L", "Female", "Married"));
        persons.add(new Person("D", "Female", "Married"));
        persons.add(new Person("M", "Male", "Single"));
        persons.add(new Person("B", "Male", "Single"));

        Criteria male = new CriteriaMale();
        Criteria female = new CriteriaFemale();
        Criteria single = new CriteriaSingle();
        Criteria singleMale = new AndCriteria(single, male);
        Criteria singleOrFemale = new OrCriteria(single, female);

        System.out.println("Males: ");
        printPersons(male.meetCriteria(persons));

        System.out.println("\nFemales: ");
        printPersons(female.meetCriteria(persons));

        System.out.println("\nSingle Males: ");
        printPersons(singleMale.meetCriteria(persons));

        System.out.println("\nSingle or Females: ");
        printPersons(singleOrFemale.meetCriteria(persons));
    }

    public static void printPersons(List<Person> persons) {
        for (Person person : persons) {
            System.out.println("Person : [ Name : " + person.getName() + ", Gender: " + person.getGender()
                    + ", Martial Status: " + person.getMartialStatus() + "]");
        }
    }
}