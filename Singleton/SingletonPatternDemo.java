public class SingletonPatternDemo {
    public static void main(String[] args){
        SingleObject obj1 = SingleObject.getInstance();
        SingleObject obj2 = SingleObject.getInstance();
        obj1.showMessage();
        obj2.showMessage();
    }
}