public class SingleObject {
    private int i = 0;

    private static SingleObject instance = new SingleObject();

    private SingleObject() {
    }

    public static SingleObject getInstance() {
        instance.i++;
        System.out.println(instance.i);
        return instance;
    }

    public void showMessage() {
        System.out.println("Hello world");
    }
}